package de.paxii.clarinet.module.external;

import com.google.common.collect.UnmodifiableIterator;

import de.paxii.clarinet.Wrapper;
import de.paxii.clarinet.event.EventHandler;
import de.paxii.clarinet.event.events.game.IngameTickEvent;
import de.paxii.clarinet.module.Module;
import de.paxii.clarinet.module.ModuleCategory;
import de.paxii.clarinet.util.module.settings.ValueBase;

import net.minecraft.block.properties.PropertyEnum;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.WorldType;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Lars on 17.10.2015.
 */
public class ModuleBlockPartyBot extends Module {
  private ValueBase scanValue;

  private HashMap<Integer, String> clayData;

  public ModuleBlockPartyBot() {
    super("BlockPartyBot", ModuleCategory.WORLD);

    this.setVersion("1.0");
    this.setBuildVersion(15801);

    this.scanValue = new ValueBase("BlockParty Radius", 40.0F, 5.0F, 100.0F);

    this.clayData = new HashMap<>();

    clayData.put(0, "white");
    clayData.put(1, "orange");
    clayData.put(2, "magenta");
    clayData.put(3, "lightBlue");
    clayData.put(4, "yellow");
    clayData.put(5, "lime");
    clayData.put(6, "pink");
    clayData.put(7, "gray");
    clayData.put(8, "silver");
    clayData.put(9, "cyan");
    clayData.put(10, "purple");
    clayData.put(11, "blue");
    clayData.put(12, "brown");
    clayData.put(13, "green");
    clayData.put(14, "red");
    clayData.put(15, "black");
  }

  @Override
  public void onEnable() {
    Wrapper.getEventManager().register(this);
  }

  @EventHandler
  public void onIngameTick(IngameTickEvent event) {
    int blockColor = this.getCurrentBlock();
    String colorName = this.getColorName(blockColor);
    double closestDistance = Double.MAX_VALUE;
    BlockPos closestBlock = null;

    if (blockColor != -1) {
      for (int x = -((int) this.scanValue.getValue()); x < (int) this.scanValue.getValue(); x++) {
        for (int z = -((int) this.scanValue.getValue()); z < (int) this.scanValue.getValue(); z++) {
          int posX = (int) Wrapper.getPlayer().posX + x;
          int posY = (int) Wrapper.getPlayer().posY - 1;
          int posZ = (int) Wrapper.getPlayer().posZ + z;

          BlockPos blockPos = new BlockPos(posX, posY, posZ);

          if (this.isRightColor(blockPos, colorName)) {
            double distanceToBlock = Wrapper.getPlayer().getDistanceSqToCenter(blockPos);

            if (distanceToBlock < closestDistance) {
              closestDistance = distanceToBlock;
              closestBlock = blockPos;
            }
          }
        }
      }

      if (closestBlock != null) {
        float[] angles = this.getAngles(closestBlock);
        Wrapper.getPlayer().rotationYaw = angles[0];

        KeyBinding.setKeyBindState(Wrapper.getGameSettings().keyBindForward.getKeyCode(), true);
      } else {
        KeyBinding.setKeyBindState(Wrapper.getGameSettings().keyBindForward.getKeyCode(), false);
      }
    } else {
      KeyBinding.setKeyBindState(Wrapper.getGameSettings().keyBindForward.getKeyCode(), false);
    }
  }

  private int getCurrentBlock() {
    ItemStack it = Wrapper.getPlayer().openContainer.getSlot(
            40).getStack();

    if (it != null) {
      int dataValue = it.getMetadata();

      return dataValue;
    }

    return -1;
  }

  private int getColorID(String colorName) {
    for (Map.Entry<Integer, String> entry : this.clayData.entrySet()) {
      if (entry.getValue().equals(colorName))
        return entry.getKey();
    }

    return -1;
  }

  private String getColorName(int colorID) {
    return this.clayData.get(colorID);
  }

  private float[] getAngles(final BlockPos blockPos) {
    double difX = (blockPos.getX() + 0.5D) - Wrapper.getPlayer().posX, difY = (blockPos.getY() + 0.5D)
            - (Wrapper.getPlayer().posY + Wrapper.getPlayer()
            .getEyeHeight()), difZ = (blockPos.getZ() + 0.5D)
            - Wrapper.getPlayer().posZ;
    double helper = Math.sqrt(difX * difX + difZ * difZ);
    float yaw = (float) (Math.atan2(difZ, difX) * 180 / Math.PI) - 90;
    float pitch = (float) -(Math.atan2(difY, helper) * 180 / Math.PI);
    return (new float[]{yaw, pitch});
  }

  private boolean isRightColor(BlockPos blockPos, String colorName) {
    IBlockState var11 = Wrapper.getWorld().getBlockState(blockPos);

    if (Wrapper.getWorld().getWorldType() != WorldType.DEBUG_WORLD) {
      var11 = var11.getBlock().getActualState(var11, Wrapper.getWorld(), blockPos);
    }

    UnmodifiableIterator var12 = var11.getProperties().entrySet().iterator();

    while (var12.hasNext()) {
      Map.Entry entry = (Map.Entry) var12.next();

      if (entry.getKey() instanceof PropertyEnum) {
        PropertyEnum pe = (PropertyEnum) entry.getKey();

        if (entry.getValue() instanceof EnumDyeColor) {
          EnumDyeColor dyeColor = (EnumDyeColor) entry.getValue();

          if (pe.getName().equals("color")) {
            if (dyeColor.getName() == colorName) {
              return true;
            }
          }
        }
      }
    }

    return false;
  }

  @Override
  public void onDisable() {
    Wrapper.getEventManager().unregister(this);
  }
}
